## CICD LAB01 - DOC

En este documento se recogen las especificaciones y evidencias del laboratorio denominado **TDD and Continuous integration  with docker, python flask, gitlab and herokuX**.


# Control de versiones

Para mantener un historial y orden claros de los cambios que se vayan haciendo en el desarrollo del proyecto se va a hacer uso de Git y su funcionalidad de *tags* para versionar.

Para esta gestión de la configuración se va a usar el **Versionado Semántico** en los *tags* de manera que cada cambio significativo del proyecto estará identificado con una versión *x.x.x*.

La versión se identifica mediante número de la siguiente manera: *Versión mayor.Versión menor.Parche*

<p align="center">
  <img src="images/versions.PNG">
</p>

Cuando se publique una nueva versión en este repositorio, se anotará en el [README.md](README.md) en el apartado **Versiones** con el siguiente formato:

**[versión]** - *fecha de publicación*
- lista detallada de cambios realizados 


# Gestión agile

Para la gestión y desarrollo del software se ha elegido SCRUM. El proyecto se ha dividido en 2 sprints (milestones) con 3 epics que se componen de 6 tasks(issues) en total. He planteado el proyecto de esta manera porque las tareas ya estaban divididas en la descripción del laboratorio.

Los tasks se colocan en un kanban con los Tags de *to-do*, *doing* o *closed* según su estado en el desarrollo y cuando todas las tareas asignadas a un sprint se cierren, este también se terminará.

# Evidencias

En este apartado se muestran los pantallazos de los resultados de cada paso desarrollado como evidencias:

- Paso 1: Comprobación de que el endpoint /ping funciona en navegador y terminal.

<p align="center">
  <img src="images/paso1-2-ping_browser.PNG">
</p>
<p align="center">
  <img src="images/paso1-3-ping_terminal.PNG">
</p>

- Paso 2: Comprobación del endpoint /ping desplegado en un contenedor con entorno de desarrollo.

<p align="center">
  <img src="images/paso2-2-ping_browser.PNG">
</p>
<p align="center">
  <img src="images/paso2-3-dev_env_docker.PNG">
</p>

- Paso 3: Comprobación de la base de datos dockerizada creada para el servicio web.

<p align="center">
  <img src="images/paso3-1-dockerized_db.PNG">
</p>

- Paso 4: Testeo funcional del servicio Ping con el framework PyTest.

<p align="center">
  <img src="images/paso4-4-ping_test.PNG">
</p>

-Paso 5: Comprobación de la aplicación refactorizada con Flask Blueprints.

<p align="center">
  <img src="images/paso5-1-tests_blueprint.PNG">
</p>
<p align="center">
  <img src="images/paso5-2-recreate_db.PNG">
</p>

Paso 6: Testeo de los endpoints del servicio User con metodologías TDD y 100% de cobertura.

<p align="center">
  <img src="images/paso6-6-final_tests.PNG">
</p>

Paso 7: Comprobación del despliegue de la aplicación en el servidor de producción Heroku.

<p align="center">
  <img src="images/paso7-5-heroku_post_cmd.PNG">
</p>
<p align="center">
  <img src="images/paso7-6-heroku_post_browser.PNG">
</p>
<p align="center">
  <img src="images/paso7-7-heroku.PNG">
</p>

Paso 8: Comprobación de la cobertura del código.

<p align="center">
  <img src="images/paso8-6-total_coverage.PNG">
</p>

Paso 9: Comprobación de la ejecución del pipeline con tres fases: buld, test y delivery.

<p align="center">
  <img src="images/paso9-2-pipeline_OK.PNG">
</p>

Paso 10: Comprobación de la ejecución del pipeline con cuatro fases: buld, test, delivery y deploy.

<p align="center">
  <img src="images/paso10-1-heroku_deploy_pipeline.PNG">
</p>
<p align="center">
  <img src="images/paso10-2-heroku_deploy.PNG">
</p>

Paso 11: Comprobación de la implementación del DELETE y PUT en /User.

<p align="center">
  <img src="images/paso11-1-user_delete_test.PNG">
</p>
<p align="center">
  <img src="images/paso11-2-user_put_test.PNG">
</p>

Paso 12: Comprobación de los tests unitarios con mocks.

<p align="center">
  <img src="images/paso12-1-unit_tests.PNG">
</p>