# CICD LAB01 y LAB02 - TDD and Continuous integration  with docker, python flask, gitlab and herokuX
 ![Made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-success)

El objetivo de este proyecto es aprender a desarrollar un proyecto web de Python con el método Test Driven Development, haciendo uso de la metodología SCRUM para la gestión y desarrollo del proyecto.Todo esto siguiendo la dinámica de desarrollo y despliegue continuo.

El proyecto es una aplicación web contenedorizada con los siguientes endpoints:
|Endpoint | HTTP Method | CRUD Method | Result |
|---|---|---|---|
| /ping | GET | - | GET succes response |
| /users | GET | READ | GET all users |
| /users/:id | GET | READ | GET a user |
| /users | POST | CREATE | ADD a user |
| /users/:id | DELETE | DELETE | DELETE a user |
| /users/:id | PUT | UPDATE | UPDATE a user |

Para el correcto desarrollo de cada una de las rutas se han seguido unos psos del TDD:
1. Escribir el Test
2. Ejecutar el Test y fallar (ESTADO ROJO)
3. Escribir el mínimo código posible para pasar el test(ESTADO VERDE)
4. Refactorizar el código si se ve necesario

El proyecto está configurado para ser testeado y desplegado automáticamente mediante un pipeline.


## Versiones

Con el desarrollo del laboratorio se irán publicando nuevas release del proyecto. A continuación las versiones liberadas y sus principales características:

**v1.0** - *19/02/2021*

- Versión inicial del proyecto. Aplicación flask con un endpoint (/ping) que responde con un mensaje (200 - OK) cuando se le hace request desde el navegador.
- Se ha configurado un entorno virtual con Python3.
- Se han creado ficheros para la configuración de la aplicación durante el desarrollo y para las dependencias requeridas.

**v2.0** - *20/02/2021*
- Versión dockerizada de la aplicación, con entorno de desarrollo configurado.

**v3.0** - *20/02/2021*
- Se ha añadido una base de datos PostgreSql deckerizada que guarda datos de usurarios.
- Se ha modificado la aplicación de la anterior versión para enlazarlo con la base de datos.

**v4.0** - *20/02/2021*
- Se han creado tests unitarios para verificar las distintas configuraciones de la aplicación.
- Se ha creado un test funcional para comprobar el servicio *Ping*.

**v5.0** - *22/02/2021*
- Se ha modificado el servicio ping como API con Flask Blueprint.
- Se ha creado un modelo de base de datos con la tabla *users* en la carpeta API.
- Se ha modificado *conftest.py* para probar los cambios anteriores.

**v6.0** - *23/02/2021*
- Se han agregado endpoints con para el servicio *User* usando la metología Test Driven Development.
- Rutas REST:
  - GET /users - Devuelve una lista con la información de todos los usuarios de la base de datos.
  - GET /users/:id - Devuelve la información del usuario identificado con el idal proporcionado.
  - POST /users - Se crea un nuevo usuario en la base de datos con la información proporcionada.

**v7.0** - *01/03/2021*
- Se ha creado un nuevo Dockerfile para el despliegue en producción junto con el servidor de producción Gunicorn.
- Se ha desplegado la aplicación en Heroku.

**v8.0** - *01/03/2021*
- Se han añadido dependencias para testear la cobertura del código:
  - *pytest-cov* para los testeos
  - *flake8* para verificación de código y errores de programación
  - *black* para formatear el código automáticamente
  - *isort* para ordenar alfabéticamente todos los import

**v9.0** - *04/03/2021*
- Se ha configurado un pipeline con runners de Gitlab para el build, test y delivery de la aplicación.
- Se ha añadido el historial de commits en el README.md

**v10.0** - *04/03/2021*
- Se ha añadido el deploy automático a Heroku en el pipeline.

**v11.0** - *04/03/2021*
- Se ha añadido la api PUT para el servicio /User usando la metodología TDD.
 - Se ha añadido la api DELETE para el servicio /User usando la metodología TDD.

**v11.1** - *07/03/2021* (Branch GIDA_11)
- Se ha creado una capa de servicio *services.py* para el acceso a la base de datos.
- Se han implementado tests unitarios con mocks.

**v11.1.1** - *07/03/2021*
- Se ha refactorizado *users.py* para pasar el test de calidad del código.


## Historial de commits

Origin/Master - [![pipeline status](https://gitlab.com/joseba.carnicero/tdd-lab/badges/master/pipeline.svg)](https://gitlab.com/joseba.carnicero/tdd-lab/commits/master)


## Documentación

Los detalles de las especificaciones, diseño y evidencias del desarrollo del laboratorio se recogen en la documentación complementaria [DOC.md](DOC.md).


## Autores

Joseba Carnicero Padilla – [joseba.carnicero@alumni.mondragon.edu](mailto:joseba.carnicero@alumni.mondragon.edu)


##  Licencia

Este proyecto está bajo Licencia [MIT License](https://choosealicense.com/licenses/mit/). Ver documento [LICENSE.md](LICENSE.md) adjunto en el proyecto para más detalles.